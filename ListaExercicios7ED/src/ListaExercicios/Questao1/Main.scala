package ListaExercicios.Questao1
object Main {
  def main(args : Array[String]){

		val numberOfElements = 10
		
		//1 - Criar a Estrutura de Dados
		var myCollectionSet = new ColectionSet(numberOfElements)
		var elements = new Array[ElementSet](numberOfElements)
		for(i<-0 until numberOfElements){
		  elements(i) = new ElementSet()
		  elements(i).valor = i
		}
		
		myCollectionSet.values = elements
		myCollectionSet.makeSet()
		
		//2 - Fazer uniao entre dois conjuntos em que pertencem x e y
		myCollectionSet.union(4, 6) match {
		  case true => println("Uniao realizada com sucesso")
		  case false => println("Elemento x nao existe")
		}
		
		//3 - Recuperar o representante de um determinado elemento
		if(myCollectionSet.find(8) != -1){
		  println("O representante e " + myCollectionSet.find(8))
		}else{
		  println("Elemento nao existe!")
		}
		
		
		//4 - Verificar se dois elementos x e y fazem parte do mesmo conjunto
		myCollectionSet.verify(4, 6) match {
		  case true => println("Os elementos fazem parte do mesmo conjunto!")
		  case false => println("Os elementos nao fazem parte do mesmo conjunto!")
		}

		//5 - Liberar a estrutura de dados
		myCollectionSet.free()
  }
}