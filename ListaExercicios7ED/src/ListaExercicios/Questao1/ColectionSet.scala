package ListaExercicios.Questao1

class ElementSet(var valor:Int = 0, var root:Int = 0) 

class ColectionSet(var values : Array[ElementSet]) {

	def this(n : Int){
		this(new Array[ElementSet](n))
		for(i<-0 until values.length) values(i) = new ElementSet
	}


	def makeSet(){
	  values.foreach {x => x.root = x.valor}
	}

	def find(x : Int) : Int = {
			values.foreach{v => if(v.valor.==(x)) return v.root}
			return -1  
	}

	def union(x : Int, y : Int) : Boolean = {
			var rootAux = find(x)
			if(rootAux.!=(-1)){
				values.foreach{ v => if(v.valor.==(y)){ 
				  v.root = rootAux 
				  return true} }
			}
			return false
	}

	def verify(x : Int, y : Int) : Boolean ={
	  find(x).==(find(y)) match {
	    case true => return true
	    case false => return false
	    }
	}

	def free(){
	  this.values = null
	}


}